#ifndef MYOPENGLWINDOW_H
#define MYOPENGLWINDOW_H

#include <QtWidgets>
#include <QtCore>
#include <QtGui>

class MyOpenGlWindow : public QMainWindow
{
	Q_OBJECT

public:
	MyOpenGlWindow(QWidget *parent = 0);
	~MyOpenGlWindow();
};

#endif // MYOPENGLWINDOW_H
