#include "myopenglwindow.h"
#include <QtWidgets>
#include <QtCore>
#include <QtGui>

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MyOpenGlWindow w;
	w.show();
	return a.exec();
}
