QT += core gui opengl
CONFIG += qt

SOURCES += glwidget.cpp
SOURCES += myopenglwindow.cpp
SOURCES += main.cpp

HEADERS += glwidget.h
HEADERS += myopenglwindow.h

LIBS += -L $$(QT_HOME)/lib
LIBS += -lglut
