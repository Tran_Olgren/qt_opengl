#include <QtOpenGL>

class GLWidget : public QGLWidget{
	Q_OBJECT

	QVector3D backgroundColor;
	double scale;
	QVector3D rotation;
	QVector3D translate;
	double perspectiveValue;

	bool mode2D = 0;

	QVector3D Max;
	QVector2D pixelStep;

	QPoint prevMousePos;
	qint32 mouseBut;
	
	int mainDisplayList;

public:
	GLWidget(QWidget* parent = NULL);

private:
	void genDisplayList();

protected:
	void resizeGL(qint32, qint32);
	void initializeGL();
	void paintGL();
};
