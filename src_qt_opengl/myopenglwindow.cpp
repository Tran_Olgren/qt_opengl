#include "myopenglwindow.h"
#include "GLWidget.h"

MyOpenGlWindow::MyOpenGlWindow(QWidget *parent)
	: QMainWindow(parent)
{
	GLWidget* w = new GLWidget(this);
	setCentralWidget(w);

	this->resize(QSize(800, 600));
	this->setWindowTitle("GLWindow");
}

MyOpenGlWindow::~MyOpenGlWindow()
{

}
