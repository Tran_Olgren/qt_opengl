#include "GLWidget.h"
#include <GL/glu.h>
#include <GL/gl.h>

GLWidget::GLWidget(QWidget* parent) : QGLWidget(parent){
	backgroundColor = QVector3D(0.9, 0.9, 0.9);
	scale = 1.0;
	
	rotation = QVector3D(0.0, 0.0, 0.0);
	translate = QVector3D(0.0, 0.0, 2.0);
	perspectiveValue = 0;

	glInit();
}

void GLWidget::resizeGL(qint32 width, qint32 height){
	if (!mode2D && perspectiveValue > 0){
		GLfloat pc;
		if (height == 0) height = 1;
		glViewport(0, 0, width, height);
		pc = (GLfloat)width / (GLfloat)height;
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		gluPerspective(perspectiveValue, 1.0*pc, 1, 100);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		Max.setY(sin(3.14159 / 360.0 * perspectiveValue) / cos(3.14159 / 360.0 * perspectiveValue));
		Max.setX(Max.y() * pc);
		pixelStep.setX(Max.x() * 2.0 / (GLdouble)width);
		pixelStep.setY(Max.y() * 2.0 / (GLdouble)height);
	}
	else{
		GLfloat pc;
		if (height == 0) height = 1;
		glViewport(0, 0, width, height);
		pc = (GLfloat)width / (GLfloat)height;
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glOrtho(-0.5 * pc, 0.5 * pc, -0.5, 0.5, 1.0, 100.0);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		Max.setY(0.5);
		Max.setX(Max.y() * pc);
		pixelStep.setX(Max.x() * 2.0 / (GLdouble)width);
		pixelStep.setY(Max.y() * 2.0 / (GLdouble)height);
	}
}
void GLWidget::initializeGL(){
	glClearColor(
		backgroundColor.x(),
		backgroundColor.y(),
		backgroundColor.z(),
		0.0);
}
void GLWidget::paintGL(){
	glLoadIdentity();

	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
	glShadeModel(GL_SMOOTH);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glEnable(GL_BLEND);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	gluLookAt(
		0.0, 0.0, -2.0,
		0.0, 0.0, 0.0,
		0.0, 1.0, 0.0);



	glColor3f(0,0,0);
	glBegin(GL_QUADS);
		glVertex3f(-0.25,  0.25, 0.0);
		glVertex3f(-0.25, -0.25, 0.0);
		glVertex3f( 0.25, -0.25, 0.0);
		glVertex3f( 0.25,  0.25, 0.0);
	glEnd();
}
