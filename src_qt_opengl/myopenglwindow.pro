QT += core gui opengl widgets
CONFIG += qt

SOURCES += glwidget.cpp
SOURCES += myopenglwindow.cpp
SOURCES += main.cpp

HEADERS += GLWidget.h
HEADERS += myopenglwindow.h

LIBS += -L $$(QT_HOME)/lib
LIBS += -lglut

